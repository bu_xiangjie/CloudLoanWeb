# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2022/10/9 14:14
@describe: 
"""

from selenium.webdriver.common.by import By


class Elements:

	def saas_elements(self, name: str) -> tuple:
		elements = {
			"collapsed_up": (By.XPATH, "//a[@class='checkbox-action down']"),
			"collapsed_down": (By.XPATH, "//a[@class='checkbox-action up']"),
			"label_all": (By.XPATH, "//span[text()=' 全部 ']"),
			"apply_product_list": (By.XPATH, "//div[@class='ant-checkbox-group']/label"),
			"search": (By.XPATH, "//span[text()='搜索']"),
			"reset": (By.XPATH, "//span[text()='重置']"),
			"search_detail": (By.XPATH, "//table[@class='ant-table-fixed']/tbody/tr"),
			"date_clean": (By.XPATH, "//span[@class='ant-calendar-picker-icon']"),
		}

		return elements.get(name, None)

	def get_menu_checkbox(self, name: str) -> tuple:
		element = (By.XPATH, f"//span[text()='{name}']/preceding-sibling::span")

		return element
