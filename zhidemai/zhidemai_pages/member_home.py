__author__ = 'buxiangjie'
"""
@time: 2024/2/9 23:06
"""

import allure

from common.base import Base
from selenium.webdriver.common.by import By


class MemberHome(Base):

	@allure.step("检查个人主页文章标题")
	def check_article_name(self, article_title: str):
		"""
		检查文章标题
		"""
		assert self.find_element(*(By.XPATH, f"//a[text()='{article_title}']"))
