__author__ = 'buxiangjie'

"""
@time: 2024/2/9 22:50
"""

import allure

from zhidemai.zhidemai_pages.index import Index


class TestIndex:

	@allure.title("搜索用户并进入个人主页")
	def test_search_user_to_member_home(self, drivers):
		"""搜索用户并进入个人主页"""
		index = Index(drivers)
		index.open()
		search_result_page = index.search("值友4359954150")
		member_home_page = search_result_page.click_member_tab_to_member_home()
		member_home_page.check_article_name("拥抱新技术，iPhone15 或将引领智能手机新时代！")

	@allure.title("搜索文章并进入文章详情")
	def test_search_article_to_article_detail(self, drivers):
		"""搜索文章并进入文章详情"""
		index = Index(drivers)
		index.open()
		search_result_page = index.search("测试")
		article_detail_page = search_result_page.click_community_tab_to_article_detail()
		article_detail_page.check_article_detail_tip_btn()
