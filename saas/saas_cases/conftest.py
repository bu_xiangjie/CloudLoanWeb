# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-20 13:35
@describe: 
"""

import os
import sys

import allure
import pytest

# 把当前目录的父目录加到sys.path中
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from datetime import datetime
from selenium import webdriver
from common.base import Base, Option
from common.login import Login
from log.ulog import logger_


def pytest_addoption(parser):
	"""
	接收命令行参数
	共有3个
	env: 运行环境
	platform: 运行系统
	browser: 浏览器名称
	"""
	parser.addoption("--env", default="saas_qa", choices=["saas_qa", "saas_test"], help="运行环境")
	parser.addoption("--platform", default="win", help="运行系统类型")
	parser.addoption("--browser", default="chrome", help="浏览器名称")


def pytest_configure(config):
	global env, platform, browser

	env = config.getoption("--env")
	platform = config.getoption("--platform")
	browser = config.getoption("--browser")


def pytest_collectstart(collector):
	collector._nodeid = f"{collector._nodeid}_{browser}"


def pytest_itemcollected(item):
	item._nodeid = f"{item._nodeid}_{browser}"

@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item):
	"""
	当测试失败的时候，自动截图，展示到html报告中
	:return:
	"""
	outcome = yield
	report = outcome.get_result()
	if report.when == "setup":
		allure.dynamic.feature(f"SAAS系统测试_{platform}_{browser}")
	if report.when == "call" or report.when == "setup":
		xfail = hasattr(report, 'wasxfail')
		if (report.skipped and xfail) or (report.failed and not xfail):
			_capture_screenshot()


def _capture_screenshot():
	"""截图保存为base64"""
	now_time = datetime.now().strftime("%Y%m%d%H%M%S")
	if not os.path.exists("./screenshot"):
		os.makedirs("screenshot")
	screen_path = os.path.join("screenshot", f"{now_time}.png")
	driver.save_screenshot(screen_path)
	allure.attach.file(screen_path, f"测试失败截图...{now_time}", allure.attachment_type.PNG)


@pytest.fixture(scope="session")
@allure.step("打开浏览器")
def drivers(request):
	global driver
	plat = Option(platform, browser)
	try:
		logger_.info("开始打开浏览器")
		driver = webdriver.Remote(
			# 设定Node节点的URL地址，后续将通过访问这个地址连接到Node计算机
			command_executor=plat.PLATFORM,  # 要和节点机显示的ip地址一样
			desired_capabilities=plat.DESIRED,
			options=plat.OPTION
		)
		logger_.info("打开浏览器成功")
		driver.maximize_window()
	# driver.set_window_size(1920, 1080)
	except Exception as e:
		raise e
	@allure.step("关闭浏览器")
	def fn():
		driver.quit()

	request.addfinalizer(fn)
	return driver


@allure.step("登录神卫跳转SAAS系统")
@pytest.fixture(scope="module", autouse=True)
def login(drivers):
	Base(driver=driver, url=env).open()
	Login(driver=driver, url=env).login(env)


@pytest.fixture(scope="function")
def back_business_inquiry(request):
	@allure.step("测试用例结束后初始化业务查询")
	def fn():
		# Index(driver).hidden_menu("0")
		driver.refresh()

	request.addfinalizer(fn)


@pytest.fixture(scope="function")
def back_financial_statistics(request):
	@allure.step("测试用例结束后初始化财务统计")
	def fn():
		# Index(driver).hidden_menu("1")
		driver.refresh()

	request.addfinalizer(fn)


@pytest.fixture(scope="function")
def back_business_management(request):
	@allure.step("测试用例结束后初始化业务管理")
	def fn():
		# Index(driver).hidden_menu("2")
		driver.refresh()

	request.addfinalizer(fn)


@pytest.fixture(scope="function")
def back_business_statistics(request):
	@allure.step("测试用例结束后初始化业务统计")
	def fn():
		# Index(driver).hidden_menu("3")
		driver.refresh()

	request.addfinalizer(fn)
