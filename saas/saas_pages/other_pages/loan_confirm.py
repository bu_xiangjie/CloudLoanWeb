# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2021-07-05 14:05:00
@describe:  还款确认页面
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class LoanConfirm(Base):
	apply_sum = (By.XPATH, "//div[text()='借款申请金额汇总：']")

	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.apply_sum)
