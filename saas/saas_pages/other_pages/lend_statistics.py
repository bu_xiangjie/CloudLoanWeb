# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-24 17:15:00
@describe: 放款统计
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class LendStatistics(Base):
	lend_success_amount = (By.XPATH, "//div[text()='放款成功总金额']")

	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.lend_success_amount)
