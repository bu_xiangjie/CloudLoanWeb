# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-24 16:30:00
@describe: 进件
"""

import allure
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebElement
from typing import List
from log.ulog import Ulog
from selenium.webdriver.support import expected_conditions as EC

from common.base import Base

log = Ulog().logger_()


class Apply(Base):
	apply_sum_amount = (By.XPATH, "//div[text()='进件总金额：']")
	clean_date = (By.XPATH, "//span[@class='ant-calendar-picker-icon']")

	def __init__(self, driver):
		super().__init__(driver)
		assert self.find_element(*self.apply_sum_amount)

	@allure.step("检查查询结果是否正确")
	def check_search_result(self, ele: List[WebElement], product_name: str):
		if ele:
			while True:
				log.info("判断元素是否可点击")
				if self.find_element(*self.saas_elements("search")).is_enabled():
					log.info("元素刷新成功，跳出循环")
					break
			for e in ele:
				name = e.find_elements(by=By.XPATH, value="td")[2].text
				assert name == product_name
		else:
			self.skip_case("无数据，跳过用例")

	@allure.step("检查进件列表与进件详情")
	def check_apply_list_and_detail(self):
		try:
			self.element_click(*self.saas_elements("collapsed_up"))
		except Exception:
			pass
		product_list = self.find_elements(*self.saas_elements("apply_product_list"))[0:7]
		self.element_click(*self.clean_date)
		for prod in product_list:
			prod.find_element(By.XPATH, "span[1]").click()
			product_name = prod.find_element(By.XPATH, "span[2]").text
			self.element_click(*self.saas_elements("search"))
			# 获取筛选结果
			time.sleep(1)
			search_detail = self.find_elements(*self.saas_elements("search_detail"))
			self.check_search_result(search_detail, product_name)
			search_detail[0].find_elements(By.XPATH, "td")[0].click()
			ApplyDetail(self.driver, product_name)
			self.driver.back()
			self.element_click(*self.saas_elements("reset"))


class ApplyDetail(Base):
	company_name = (By.XPATH, "//span[text()='公司名称：']")
	customer_name = (By.XPATH, "//span[text()='姓名：']")

	def __init__(self, driver, y):
		super().__init__(driver)
		if y == "订车贷":
			self.find_element(*self.company_name)
		else:
			self.find_element(*self.customer_name)
