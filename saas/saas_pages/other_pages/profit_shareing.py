# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-26 09:35:00
@describe: 分润明细
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class ProfitShareing(Base):
	sum = (By.XPATH, "//div[text()='分润金额汇总：']")

	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.sum)
