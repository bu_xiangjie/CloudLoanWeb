# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-26 09:30:00
@describe: 债转
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class Swap(Base):
	pay_amt = (By.XPATH, "//div[text()='实还债转汇总：']")

	@allure.step("")
	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.pay_amt)
