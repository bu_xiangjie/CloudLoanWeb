# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-25 16:37:00
@describe: 操作日志
"""

import allure
from selenium.webdriver.common.by import By
from log.ulog import Ulog

from common.base import Base

log = Ulog().logger_()


class OperateLog(Base):

	@allure.step("进入操作日志并检查")
	def __init__(self, driver):
		log.info("--->断言是否成功跳转操作日志页面")
		super().__init__(driver=driver)
		self.operate_log = (By.XPATH, "//span[text()='日志编号']")
		self.detail = (By.XPATH, "//a[text()='详情']")
		assert self.get_text(*self.operate_log) == "日志编号"

	@allure.step("进入操作日志详情并检查")
	def page_operate_log_detail(self):
		self.find_elements(*self.detail)[0].click()
		return OperateLogDetail(self.driver)


class OperateLogDetail(Base):
	def __init__(self, driver):
		log.info("--->断言是否成功跳转日志详情页面")
		super().__init__(driver)
		self.operate_log_detail = (By.XPATH, "//span[text()='日志详情']")
		assert self.get_text(*self.operate_log_detail) == "日志详情"
