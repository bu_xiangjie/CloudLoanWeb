# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-26 09:20:00
@describe: 还款(原分账)
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class Repayment(Base):
	pay_amt = (By.XPATH, "//div[text()='实还金额汇总：']")

	@allure.step("检查还款是否跳转成功")
	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.pay_amt)
