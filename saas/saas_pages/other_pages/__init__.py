# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2022/9/29 16:59
@describe: 
"""
from saas.saas_pages.other_pages.apply import Apply
from saas.saas_pages.other_pages.asset_list import AssetList
from saas.saas_pages.other_pages.business_switch import BusinessSwitch
from saas.saas_pages.other_pages.capital_flow import CapitalFlow
from saas.saas_pages.other_pages.channel_product_quote import ChannelProductQuote
from saas.saas_pages.other_pages.credit import Credit
from saas.saas_pages.other_pages.loan_confirm import LoanConfirm
from saas.saas_pages.other_pages.loan_statistics import LoanStatistics
from saas.saas_pages.other_pages.operate_log import OperateLog
from saas.saas_pages.other_pages.profit_shareing import ProfitShareing
from saas.saas_pages.other_pages.profit_shareing_2019 import ProfitShareing2019
from saas.saas_pages.other_pages.quote_center import QuoteCenter
from saas.saas_pages.other_pages.lend_statistics import LendStatistics
from saas.saas_pages.other_pages.repayment import Repayment
from saas.saas_pages.other_pages.split_report import SplitReport
from saas.saas_pages.other_pages.swap import Swap
from saas.saas_pages.other_pages.swap_contract_confirm import SwapContractConfirm
from saas.saas_pages.other_pages.profit_statistics import ProfitStatistics

__all__ = ['Apply', 'AssetList', 'BusinessSwitch', 'CapitalFlow', 'ChannelProductQuote', 'Credit',
		   'LoanConfirm', 'LoanStatistics',
		   'OperateLog', 'ProfitShareing', 'ProfitShareing2019', 'QuoteCenter', 'LendStatistics',
		   'Repayment', 'SplitReport', 'Swap', 'SwapContractConfirm', 'ProfitStatistics']
