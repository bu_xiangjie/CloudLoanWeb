# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-05-12 11:26:00
@describe: 
"""
import os
import time

from loguru import logger
from pathlib import Path


class Ulog:
	"""
	初始化日志输出目录与规则
	"""
	__instance = None

	def __new__(cls, *args, **kwargs):
		if cls.__instance:
			return cls.__instance
		else:
			cls.__instance = super().__new__(cls)
			return cls.__instance

	# noinspection PyBroadException
	def __init__(self):
		self.logger = logger
		hand_list = []
		for key in self.logger._core.handlers:
			hand_list.append(key)
		while len(hand_list) >= 2:
			self.logger.remove(hand_list[-1])
			hand_list.pop()
		log_path = Path(__file__).parent.parent / "logs"
		rq = time.strftime("%Y_%m_%d", time.localtime())
		if not os.path.exists(log_path):
			os.mkdir(log_path)
		self.logger.add(log_path / f"{rq}.log", rotation="00:00", enqueue=True)

	def logger_(self):
		return self.logger


logger_: logger = Ulog().logger_()
